//
//  SRKViewController.h
//
//  Created by Protools on 7/28/15.
//  Copyright (c) 2015 KUSF.org. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "xmlOperation.h"

#import "CustomCell.h"

@interface ViewController : UIViewController<UITableViewDataSource,UITableViewDelegate,xmlOperationDelegate>

{
    
    NSMutableArray *arrayValues;
    
    IBOutlet UITableView *tblView;
    
    IBOutlet UITableViewCell *tblCell;
    
    IBOutlet UILabel *lblTitle,*lblArtist,*lblTime;
    
    IBOutlet UIImageView *imgView;
    
    xmlOperation *xmlOp;
    
}

@property(nonatomic,strong)NSDictionary *dictionaryObject;

-(IBAction)Done:(id)sender;

@end

