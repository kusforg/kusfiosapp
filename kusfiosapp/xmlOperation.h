//
//  xmlOperation.h
//  radioapp
//
//  Created by Protools on 7/28/15.
//  Copyright (c) 2015 KUSF.org. All rights reserved.
//
#import <Foundation/Foundation.h>



@protocol xmlOperationDelegate;

@interface xmlOperation : NSObject<NSXMLParserDelegate>{
    
    NSArray *arrayElements;
    
    NSMutableArray *arrayValues;
    
    NSMutableDictionary *dictionaryObject;
    
}

@property(nonatomic,strong)NSArray *arrayElements;

@property(nonatomic,strong)NSString *StrUrl;

@property(nonatomic,strong)NSString *strElement;

@property(nonatomic,strong)NSString *strValue;

@property(nonatomic,strong)NSString *strEntityRoot;

@property(nonatomic,strong)id delegate;



-(id)initWithPostRequest:(NSString *)Posturl requestedElements:(NSArray *)requestedElements rootElement:(NSString *)rootElement delegate:(id<xmlOperationDelegate>)delegates;



@end

@protocol xmlOperationDelegate <NSObject>

-(void)DidFinishXmlOperation:(NSArray *)arrayData;

@end

