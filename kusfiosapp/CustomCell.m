//
//  CustomCell.m
//  radioapp
//
//  Created by Protools on 7/28/15.
//  Copyright (c) 2015 KUSF.org. All rights reserved.
//

#import "CustomCell.h"

@implementation CustomCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
