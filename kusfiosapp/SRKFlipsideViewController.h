//
//  SRKFlipsideViewController.h
//
//  Created by Protools on 7/28/15.
//  Copyright (c) 2015 KUSF.org. All rights reserved.


#import <UIKit/UIKit.h>

@class SRKFlipsideViewController;

@protocol SRKFlipsideViewControllerDelegate
- (void)flipsideViewControllerDidFinish:(SRKFlipsideViewController *)controller;
@end

@interface SRKFlipsideViewController : UIViewController

@property (weak, nonatomic) id <SRKFlipsideViewControllerDelegate> delegate;

- (IBAction)done:(id)sender;

@end
