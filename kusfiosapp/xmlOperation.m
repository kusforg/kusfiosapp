//
//  xmlOperation.m
//  radioapp
//
//  Created by Protools on 7/28/15.
//  Copyright (c) 2015 KUSF.org. All rights reserved.
//

#import "xmlOperation.h"



@implementation xmlOperation

@synthesize arrayElements,StrUrl,delegate,

strElement,strValue;

-(id)initWithPostRequest:(NSString *)Posturl requestedElements:(NSArray *)requestedElements rootElement:(NSString *)rootElement delegate:(id<xmlOperationDelegate>)delegates{
    
    if(self==[super init]){
        
        arrayValues=[[NSMutableArray alloc]init];
        
        dictionaryObject=[[NSMutableDictionary alloc]init];
        
        self.strEntityRoot=rootElement;
        
        self.arrayElements=requestedElements;
        
        self.StrUrl=Posturl;
        
        self.delegate=delegates;
        
        [self startParsing:self.StrUrl];
        
    }
    
    return self;
    
}

-(void)startParsing:(NSString *)requestUrl{
    
    NSURL *url=[NSURL URLWithString:requestUrl];
    
    NSData *data=[NSData dataWithContentsOfURL:url];
    
    NSXMLParser *parser=[[NSXMLParser alloc]initWithData:data];
    
    parser.delegate=self;
    
    [parser parse];
    
}

#pragma mark - NSXmlParser Delegates

- (void)parser:(NSXMLParser *)parser parseErrorOccurred:(NSError *)parseError

{
    
    NSLog(@"Error: %@",[parseError localizedDescription]);
    
}

- (void)parser:(NSXMLParser *)parser foundCharacters:(NSString *)string

{
    
    NSLog(@"---%@",string);
    
    self.strValue=nil;
    
    if(string!=nil){
        
        self.strValue=string;
        
    }
    
}

-(void)parser:(NSXMLParser *)parser didEndElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName{
    
    NSLog(@"%@, and element %@",self.strValue,self.strElement);
    
    NSLog(@"root end %@",elementName);
    
    self.strElement=nil;
    
    NSLog(@"%@ -- %@",elementName,qName);
    
    if(![self.strEntityRoot isEqualToString:elementName]){
        
        for (NSString *strEl in  self.arrayElements) {
            
            if([strEl isEqualToString:elementName]){
                
                self.strElement=elementName;
                
                [dictionaryObject setObject:self.strValue forKey:self.strElement];
                
            }
            
        }
        
    }else{
        
        [arrayValues addObject:dictionaryObject];
        
        dictionaryObject=[[NSMutableDictionary alloc]init];
        
    }
    
}

- (void)parserDidEndDocument:(NSXMLParser *)parser

{
    
    NSLog(@"Document finished", nil);
    
    [self.delegate DidFinishXmlOperation:arrayValues];
    
}

@end

